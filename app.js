// Variable From UI
const selectedMovieUI = document.getElementById("movie");
const containerUI = document.querySelector(".container");
const seatsUI = document.querySelectorAll(".row .seat:not(.occupied)");
const countUI = document.getElementById("count");
const totalUI = document.getElementById("total");
// Get the selected movie price
let moviePrice = parseInt(selectedMovieUI.value);
// Loading Data whene window load
getLocalData();
selectedSeatCount()
// Event Listner
selectedMovieUI.addEventListener("change", selectedMovie);
// containerUI.addEventListener("mouseup", selectSeat);
containerUI.addEventListener("click", selectSeat);
// Select Seat Function
function selectSeat(e) {
    if(e.target.classList.contains("seat") && !e.target.classList.contains("occupied")) {
       e.target.classList.toggle("selected");
       selectedSeatCount();
    }
}

// Movie Selecet Event
function selectedMovie(e) {
    moviePrice = parseInt(e.target.value);
    const selectedMovieIndex = e.target.selectedIndex;
    selectedSeatCount(); 
    setMovieData(selectedMovieIndex, e.target.value);  
}

// Selected Seat Event
function selectedSeatCount() {    
    // Get all Selected seat count
    const selectedSeatUI = document.querySelectorAll(".row .seat.selected");
    // Getting all the selected seats array
    // const seatSelectedArr = [...selectedSeatUI].map(function(seat) {
    //     return [...seatsUI].indexOf(seat);
    // });
    const seatSelectedIndexArr = [...selectedSeatUI].map(seat => [...seatsUI].indexOf(seat));
    // Store it in local Storage
    localStorage.setItem("selectedSeat", JSON.stringify(seatSelectedIndexArr));
    const selectedSeatTotal = selectedSeatUI.length;
    const totalMoviePrice = selectedSeatTotal * moviePrice;
    countUI.textContent = selectedSeatTotal;
    totalUI.textContent = totalMoviePrice;
}

// Store Movie price and movie in the L storage

function setMovieData(movieIndex, moviePrice) {
    localStorage.setItem("selectedMovieIndex", movieIndex);
    localStorage.setItem("selectedMoviePrice", moviePrice);
}

// Get the local storage data when loaded
function getLocalData() {
    const seatSelected = JSON.parse(localStorage.getItem("selectedSeat"))
    // seatSelected.forEach(index => {
    //     seatsUI[index].classList.toggle("selected")
    // });
    // OR
    if(seatSelected !== null && seatSelected.length > 0) {
        seatsUI.forEach((seat, index) => {
            if(seatSelected.indexOf(index) > -1) {
                seat.classList.add("selected");
            }
        })
    };
    // Get movie and price
    const localMovie = localStorage.getItem("selectedMovieIndex");
    if(localMovie !== null) {
        selectedMovieUI.selectedIndex = localMovie;
    }
    const localMoviePrice = localStorage.getItem("selectedMoviePrice");
    if(localMoviePrice !== null) {
        moviePrice = parseInt(localMoviePrice)
    }
    
}
